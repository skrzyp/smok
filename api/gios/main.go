package gios

// API Głównego Inspektoratu Ochrony Środowiska
// doc: http://powietrze.gios.gov.pl/pjp/content/api

import "github.com/kellydunn/golang-geo"
import "encoding/json"
import "gitlab.com/skrzyp/smok/util"
import "strconv"

type StationEntry struct {
	ID          int         `json:"id"`
	StationName string      `json:"stationName"`
	DateStart   string      `json:"dateStart"`
	DateEnd     interface{} `json:"dateEnd"`
	GeoLat      string      `json:"gegrLat"`
	GeoLon      string      `json:"gegrLon"`
	City        struct {
		ID      int    `json:"id"`
		Name    string `json:"name"`
		Commune struct {
			CommuneName  string `json:"communeName"`
			DistrictName string `json:"districtName"`
			ProvinceName string `json:"provinceName"`
		} `json:"commune"`
	} `json:"city"`
	AddressStreet string `json:"addressStreet"`
}

const TimeFormat = "2006-01-02 15:04:05"

type StationIndex struct {
	Station []StationEntry
}

func NewIndex() StationIndex {
	var index StationIndex
	indexJSON := util.GetJSON("http://api.gios.gov.pl/pjp-api/rest/station/findAll")
	err := json.Unmarshal(indexJSON, &index.Station)
	util.HandleError(err)
	return index
}

type SensorMetrics struct {
	Key    string        `json:"key"`
	Values []SensorValue `json:"values"`
}

type SensorValue struct {
	Date  string  `json:"date"`
	Value float32 `json:"value"`
}

type StationSensors []struct {
	ID        int `json:"id"`
	StationID int `json:"stationId"`
	Param     struct {
		ParamName    string `json:"paramName"`
		ParamFormula string `json:"paramFormula"`
		ParamCode    string `json:"paramCode"`
		IDParam      int    `json:"idParam"`
	} `json:"param"`
	SensorDateStart string `json:"sensorDateStart"`
	SensorDateEnd   string `json:"sensorDateEnd"`
}

func StationToPoint(index StationIndex, stationID int) *geo.Point {
	stationLat, _ := strconv.ParseFloat(index.Station[stationID].GeoLat, 64)
	stationLon, _ := strconv.ParseFloat(index.Station[stationID].GeoLat, 64)
	stationPoint := geo.NewPoint(stationLat, stationLon)
	return stationPoint
}

func StationDistance(index StationIndex, stationID int, location geo.Point) float64 {
	stationPoint := StationToPoint(index, stationID)
	stationDistance := stationPoint.GreatCircleDistance(&location)
	return stationDistance
}

func StationToIndex(index StationIndex, stationid int) int {
	for i := len(index.Station) - 1; i >= 0; i-- {
		if index.Station[i].ID == stationid {
			return i
		}
	}
	return 0
}
