package main

import (
	"encoding/json"
	"fmt"
	"github.com/kellydunn/golang-geo"
	"gitlab.com/skrzyp/smok/api/gios"
	"gitlab.com/skrzyp/smok/util"
	"strconv"
)

var giosStationIndex = gios.NewIndex()

func getAllStations(index gios.StationIndex, lat float64, lon float64) {
	location := geo.NewPoint(lat, lon)

	for stationIndex := len(index.Station) - 1; stationIndex >= 0; stationIndex-- {
		stationLat, _ := strconv.ParseFloat(index.Station[stationIndex].GeoLat, 64)
		stationLon, _ := strconv.ParseFloat(index.Station[stationIndex].GeoLon, 64)
		stationPoint := geo.NewPoint(stationLat, stationLon)
		fmt.Printf("%06d %4.fkm %s %s %s : %s \n",
			index.Station[stationIndex].ID,
			location.GreatCircleDistance(stationPoint),
			index.Station[stationIndex].City.Commune.ProvinceName,
			index.Station[stationIndex].City.Name,
			index.Station[stationIndex].AddressStreet,
			util.GetLocator(stationLat, stationLon))
	}
}

func getStationSensors(stationID int) {
	var sensors gios.StationSensors
	jsonURL := fmt.Sprintf("http://api.gios.gov.pl/pjp-api/rest/station/sensors/%d", stationID)
	stationSensorsJSON := util.GetJSON(jsonURL)
	err := json.Unmarshal(stationSensorsJSON, &sensors)
	util.HandleError(err)
	for sensorIndex := len(sensors) - 1; sensorIndex >= 0; sensorIndex-- {
		fmt.Printf("-- %d %s (since %s)\n",
			sensors[sensorIndex].ID,
			sensors[sensorIndex].Param.ParamName,
			sensors[sensorIndex].SensorDateStart)
	}
}

func getMetricsFromSensor(sensorID int) {
	var metric gios.SensorMetrics
	jsonURL := fmt.Sprintf("http://api.gios.gov.pl/pjp-api/rest/data/getData/%d", sensorID)
	sensorMetricJSON := util.GetJSON(jsonURL)
	err := json.Unmarshal(sensorMetricJSON, &metric)
	util.HandleError(err)
	for metricIndex := len(metric.Values) - 1; metricIndex >= 0; metricIndex-- {
		if metric.Values[metricIndex].Value != 0.0 {
			fmt.Printf("   -- [%s] %f\n",
				metric.Values[metricIndex].Date,
				metric.Values[metricIndex].Value)
			break
		}
	}
}

func getStation(index gios.StationIndex, stationID int) string {
	return index.Station[gios.StationToIndex(index, stationID)].StationName
}

func main() {
	var smtr gios.SensorMetrics
	fmt.Println("Getting JSON data…")
	metricJSON := util.GetJSON("http://api.gios.gov.pl/pjp-api/rest/data/getData/2772")
	err := json.Unmarshal(metricJSON, &smtr)
	util.HandleError(err)
	mylat, mylon := util.GetLocation()
	fmt.Printf("My location: %f, %f\n", mylat, mylon)
	desiredStationIndex := 401
	getAllStations(giosStationIndex, mylat, mylon)
	getStationSensors(401)
	getMetricsFromSensor(2772)
	fmt.Printf("Current station: %s\n", getStation(giosStationIndex, desiredStationIndex))
	fmt.Printf("Current metric: %s\n", smtr.Key)
	fmt.Printf("Current value: %f\n", smtr.Values[2].Value)
	fmt.Printf("Sampling date: %s\n", smtr.Values[2].Date)
}
