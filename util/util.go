package util

import (
	"fmt"
	"github.com/kellydunn/golang-geo"
	"github.com/oschwald/geoip2-golang"
	"github.com/pd0mz/go-maidenhead"
	"github.com/rdegges/go-ipify"
	"io/ioutil"
	"net"
	"net/http"
	"os"
)

func GetLocation() (float64, float64) {
	db, err := geoip2.Open("GeoIP2-City.mmdb")
	HandleError(err)
	defer func() {
		err := db.Close()
		HandleError(err)
	}()
	ipstr, err := ipify.GetIp()
	HandleError(err)
	ip := net.ParseIP(ipstr)
	record, err := db.City(ip)
	HandleError(err)
	point := geo.NewPoint(record.Location.Latitude, record.Location.Longitude)
	HandleError(err)
	return point.Lat(), point.Lng()
}

func HandleError(err error) {
	if err != nil {
		fmt.Printf("Wystąpił bład: %s", err)
		os.Exit(127)
	}
}

func GetLocator(lat float64, lon float64) string {
	coords := maidenhead.NewPoint(lat, lon)
	locator, err := coords.Locator(maidenhead.ExtendedSquarePrecision)
	HandleError(err)
	return locator
}

func GetJSON(url string) []byte {
	resp, err := http.Get(url)
	HandleError(err)

	defer func() {
		err := resp.Body.Close()
		HandleError(err)
	}()

	body, err := ioutil.ReadAll(resp.Body)
	HandleError(err)

	return body
}
